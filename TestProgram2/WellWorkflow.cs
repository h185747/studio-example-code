﻿using Slb.Ocean.Core;
using Slb.Ocean.Geometry;
using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Well;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestProgram2
{
    class WellWorkflow
    {
        private static Repository RepoObject => RepositoryUtil.RepoObject;
        public static string GUID { get; set; }
        private static List<ExplicitTrajectoryRecord> explicitRecords;

        public void Dispose()
        {
            //GC.RemoveMemoryPressure(GC.GetTotalMemory(true));
           // GC.Collect();
        }

        public static void CreateSingleBorehole()
        {
            Console.WriteLine("Creating top level BoreholeCollection");
            BoreholeCollection topBoreholeCollection = WellRoot.Get(RepoObject).GetOrCreateBoreholeCollection();
            Console.WriteLine("Creating BoreholeCollection - 'Wells'");
            BoreholeCollection boreholeCollection = topBoreholeCollection.CreateBoreholeCollection("Wells");

            Console.WriteLine("Creating Borehole 'Well_00000'");
            Borehole borehole = boreholeCollection.CreateBorehole("Well_00000");
            borehole.WellHead = new Point2(107942.62, 5096372.17);
            borehole.WorkingReferenceLevel = new ReferenceLevel("Test", 1.0, "Test Reference Level");
            borehole.UWI = "UWI-Well_00000";
            borehole.Operator = "HA";

            CreateTrajectoryUsingExplicitRecordSet(borehole, AzimuthalReference.GridNorth);

            GUID = borehole.UniqueId;
        }

        private static void CreateTrajectoryUsingExplicitRecordSet(Borehole borehole, AzimuthalReference reference)
        {
            Console.WriteLine(string.Format("  Creating Trajectory for Borehole {0} ", borehole.Name));
            explicitRecords = new List<ExplicitTrajectoryRecord>();

            using (StreamReader reader = new StreamReader("C:\\Program Files\\Schlumberger\\Ocean for Studio 2022\\Samples\\SDK Samples\\Data\\D9Trajectory.csv"))
            {
                // Skip the header
                reader.ReadLine();
                // Read all data rows
                while (!reader.EndOfStream)
                {
                    string[] columnValues = reader.ReadLine().Split(new char[] { ',' }, StringSplitOptions.None);

                    double x = Convert.ToDouble(columnValues[0]);
                    double y = Convert.ToDouble(columnValues[1]);
                    double z = Convert.ToDouble(columnValues[2]);
                    double md = Convert.ToDouble(columnValues[3]);
                    double inclination = Angle.DegreesToRadians(Convert.ToDouble(columnValues[4]));
                    double azimuth = Angle.DegreesToRadians(Convert.ToDouble(columnValues[5]));

                    explicitRecords.Add(new ExplicitTrajectoryRecord(md, inclination, azimuth, new Point3(x, y, z)));
                }
            }

            // Creates Borehole trajectory by direct setter to ExplicitRecords
            //borehole.Trajectory.SetExplicitRecords(explicitRecords, reference);

            ExplicitTrajectory explicitTrajectory = borehole.Trajectory.TrajectoryCollection.CreateTrajectory<ExplicitTrajectory>(borehole.Name + new Random().NextDouble());
            explicitTrajectory.ExplicitTrajectoryRecords = explicitRecords;
            borehole.Trajectory.TrajectoryCollection.DefinitiveSurvey = explicitTrajectory;
        }

        private static void CreateMultipleBoreholes(int num)
        {
            Borehole origBorehole = RepoObject.ResolveUniqueId<Borehole>(GUID);
            BoreholeCollection boreholeCollection = origBorehole.BoreholeCollection;
            AzimuthalReference azmRef;
            var records = (origBorehole.Trajectory.TrajectoryCollection.WorkingTrajectory as ExplicitTrajectory).GetTrajectoryRecords();//.GetExplicitRecords(out azmRef);

            for (int i = 1; i < num; i++) // starting from 1 as one borehole already created
            {
                string name = "Well_" + i.ToString().PadLeft(5, '0');
                Console.Write("Creating Borehole '{0}' ... ", name);
                Borehole borehole = boreholeCollection.CreateBorehole(name);
                borehole.WellHead = new Point2(107942.62, 5096372.17);
                borehole.WorkingReferenceLevel = origBorehole.WorkingReferenceLevel;
                borehole.UWI = "UWI-" + name;
                borehole.Operator = "HA";

                //borehole.Trajectory.SetExplicitRecords(records, azmRef);

                Console.WriteLine("done!");
                ExplicitTrajectory explicitTrajectory = borehole.Trajectory.TrajectoryCollection.CreateTrajectory<ExplicitTrajectory>(borehole.Name + new Random().NextDouble());
                explicitTrajectory.ExplicitTrajectoryRecords = explicitRecords;
                borehole.Trajectory.TrajectoryCollection.DefinitiveSurvey = explicitTrajectory;
               // RepositoryUtil.SaveRepository(true);
            }

        }

        public static void Execute()
        {
            Console.WriteLine("\n===============================================");

            //RepositoryUtil.OpenRepository("Studio_20", "elk_tgt", true);

            //Console.ReadKey();
            //Reading all wellbores

            /*List<Borehole> boreholes = RepositoryUtil.RepoObject.Queryables.Well.AllBoreholes.ToList();
            int boreholeCount = 0;
            foreach (Borehole b in boreholes)
            {
                boreholeCount = boreholeCount + 1;
            }

            Console.WriteLine("\nTotal borehole read: " + boreholeCount);*/

            //GUID = RepoObject.Queryables.Well.AllBoreholes.FirstOrDefault().UniqueId;
            int remaining = 2000 - RepoObject.Queryables.Well.AllBoreholes.Count();
            CreateSingleBorehole();
            CreateMultipleBoreholes(remaining);

            RepositoryUtil.SaveRepository(true);

            // Console.ReadKey();
            //Console.WriteLine("\nClosing Repository\n");
            //RepositoryUtil.CloseRepository();
            //Console.WriteLine("\n Repository closed\n");
            /* Console.ReadKey();
             GC.Collect();
             Console.WriteLine("\nAfter GC.Collect()\n");
             Console.ReadKey();*/
            //GC.RemoveMemoryPressure(GC.GetTotalMemory(true));

            //Console.WriteLine("===============================================\n");
            //Console.ReadKey();
        }
    }
}
