﻿using Slb.Ocean.Core;
using Slb.Ocean.Geometry;
using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Basics;
using Slb.Ocean.Studio.Data.Petrel.UI;
using Slb.Ocean.Studio.Data.Petrel.Well;
using Slb.Ocean.Units;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class LogCurveWorkflow
    {
        private static Repository RepoObject => RepositoryUtil.RepoObject;
        private static string GUID { get; set; }

        private static LogVersionCollection GetOrCreateLogVersionCollection(string name)
        {
            WellRoot wellRoot = WellRoot.Get(RepoObject);
            LogVersionCollection logVersionCollection = RepoObject.Queryables.Well.AllLogVersionCollections.Where(x => x.Name == name).FirstOrDefault();
            if (logVersionCollection == null)
            {
                logVersionCollection = wellRoot.LogVersionCollection.CreateLogVersionCollection(name);
                RepositoryUtil.SaveRepository(true);
            }
            return logVersionCollection;
        }

        private static WellLogVersion GetOrCreateWellLogVersion(LogVersionCollection lvc, string name = "TestWellLog")
        {
            WellLogVersion wellLogVersion = RepoObject.Queryables.Well.AllWellLogVersions.Where(x => x.Name == name).FirstOrDefault();
            if (wellLogVersion == null)
            {
                //Template template = RepoObject.WellKnownTemplates.PetrophysicalTemplates.Permeability;
                var unitM = CoreSystem.GetService<IUnitServiceSettings>().CurrentCatalog.GetUnitMeasurements().FirstOrDefault(um => um.Name.Equals("UNKNOWN", StringComparison.InvariantCultureIgnoreCase));
                var templateCollection = RepoObject.Queryables.AllTemplateCollections.Where(x => x.Name == "Well log templates").FirstOrDefault();
                Template template;
                if (templateCollection.Templates.Any(x => x.Name == "UNKNOWN"))
                    template = templateCollection.Templates.First(x => x.Name == "UNKNOWN");
                else
                    template = templateCollection.CreateTemplate("UNKNOWN", unitM);
                wellLogVersion = lvc.CreateWellLogVersion(name, template);
                //RepositoryUtil.SaveRepository(true);
            }
            return wellLogVersion;
        }

        private static List<WellLogSample> ReadSamples()
        {
            List<WellLogSample> samples = new List<WellLogSample>();
            using (StreamReader reader = new StreamReader("C:\\Program Files\\Schlumberger\\Ocean for Studio 2022\\Samples\\SDK Samples\\Data\\D9Trajectory.csv"))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    string[] columnValues = reader.ReadLine().Split(new char[] { ',' }, StringSplitOptions.None);

                    double md = Convert.ToDouble(columnValues[0]);
                    float value = Convert.ToSingle(string.IsNullOrEmpty(columnValues[1]) ? null : columnValues[1]);

                    samples.Add(new WellLogSample(md, value));
                }
            }
            return samples;
        }

        private static List<WellLogSample> CreateSamples(int totalSamples = 10000, int increment = 1)
        {
            List<WellLogSample> samples = new List<WellLogSample>();
            double md = 0;
            float value = 0;
            for (int i = 0; i < totalSamples; i++, md += increment, value += 2)
            {
                samples.Add(new WellLogSample(md, value));
            }
            return samples;
        }

        private static void CreateLogCurves()
        {
            LogVersionCollection logVersionCollection = GetOrCreateLogVersionCollection("LogCurves");
            WellLogVersion wellLogVersion = GetOrCreateWellLogVersion(logVersionCollection);
            List<WellLogSample> samples = ReadSamples();
            List<Borehole> boreholes = RepoObject.Queryables.Well.AllBoreholes.ToList();
            foreach (Borehole borehole in boreholes)
            {
                Console.Out.Write("Creating WellLog for Borehole {0}...", borehole.Name);
                WellLog log = borehole.Logs.CreateWellLog(wellLogVersion);
                log.Samples = samples;
                Console.Out.Write("done! ...  ");
                //RepositoryUtil.SaveRepository(true);
            }
        }

        private static void CreateLogCurves(int startI = 0, int num = 1000)
        {
            List<WellLogSample> samples = CreateSamples();
            string boreholeGUID = "D360BF27E6C145E583DFDA4DD01FB572";
            string logCollectionGUID = "77C9A7BAC9E2432B97707E332A3BB46B";

            DateTime start = DateTime.Now;

            for (int i = startI; i < startI + num; i++)
            {
                DateTime t1 = DateTime.Now;

                RepositoryUtil.OpenRepository("petrel2", "perf1");
                Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(boreholeGUID);
                if (borehole == null)
                    borehole = RepoObject.Queryables.Well.AllBoreholes.FirstOrDefault();
                LogVersionCollection logVersionCollection = RepoObject.ResolveUniqueId<LogVersionCollection>(logCollectionGUID);
                if (logVersionCollection == null)
                    logVersionCollection = RepoObject.Queryables.Well.AllLogVersionCollections.FirstOrDefault();
                Template template = RepoObject.WellKnownTemplates.MiscellaneousTemplates.General;

                string wellLogName = "Curve_" + i.ToString().PadLeft(5, '0');
                WellLogVersion logVersion = logVersionCollection.CreateWellLogVersion(wellLogName, template);
                WellLog wellLog = borehole.Logs.CreateWellLog(logVersion);
                wellLog.Samples = samples;
                RepositoryUtil.SaveRepository();
                RepositoryUtil.CloseRepository();

                double t = (DateTime.Now - t1).TotalSeconds;
                Console.Out.WriteLine("{0} written in : {1} seconds", wellLogName, t);
            }
            //RepositoryUtil.CloseRepository();
            double time = Math.Round((DateTime.Now - start).TotalSeconds, 3);
            Console.Out.WriteLine("\n{0} LogCurves written in {1} seconds", num, time);
        }

        private static void CreateLogCurves_v2(string boreholeName, int startI = 0, int num = 1000)
        {
            List<WellLogSample> samples = CreateSamples();
           // string boreholeGUID = "D360BF27E6C145E583DFDA4DD01FB572";
            //string logCollectionGUID = "77C9A7BAC9E2432B97707E332A3BB46B";
            LogVersionCollection logVersionCollection;

            WellRoot wellRoot = WellRoot.Get(RepositoryUtil.RepoObject);
            logVersionCollection = wellRoot.LogVersionCollection;

            DateTime start = DateTime.Now;

            List<Borehole> boreholes = RepoObject.Queryables.Well.AllBoreholes.Where(x=>x.Name.Contains(boreholeName)).ToList();
            foreach(Borehole borehole in boreholes)
            {
                logVersionCollection = logVersionCollection.CreateLogVersionCollection(borehole.Name);

                for (int i = startI; i < startI + num; i++)
                {
                    DateTime t1 = DateTime.Now;

                    if (logVersionCollection == null)
                        logVersionCollection = RepoObject.Queryables.Well.AllLogVersionCollections.FirstOrDefault();
                    Template template = RepositoryUtil.RepoObject.WellKnownTemplates.MiscellaneousTemplates.General;

                    string wellLogName = "Curve_" + i.ToString().PadLeft(5, '0');
                    WellLogVersion logVersion = logVersionCollection.CreateWellLogVersion(wellLogName, template);
                    WellLog wellLog = borehole.Logs.CreateWellLog(logVersion);
                    wellLog.Samples = samples;
                    //RepositoryUtil.SaveRepository();
                    //RepositoryUtil.CloseRepository();

                    double t = (DateTime.Now - t1).TotalSeconds;
                    Console.Out.WriteLine("{0} written in : {1} seconds", wellLogName, t);
                }
            }

            
            //RepositoryUtil.CloseRepository();
            double time = Math.Round((DateTime.Now - start).TotalSeconds, 3);
            Console.Out.WriteLine("\n{0} LogCurves written in {1} seconds", num, time);
        }




        public static void WriteWellLogList(Borehole borehole, LogVersionCollection logVersionCollection)
        {
            //try
            //{
            //    if (borehole != null)
            //    {
            //        DictionaryTemplate dictionaryTemplate;
            //        Template template;

            //        Console.Out.WriteLine(DateTime.Now + " -  Writing LogCurve for borehole with UWI: " + borehole.UWI);

            //        CreateTemplate(, , out dictionaryTemplate, out template);

            //        //If there is dictionary template or IsDictionaryWellLog is true then Create Dictionary well Log
            //        if (PetrelStudioWellLog.IsDictionaryWellLog || dictionaryTemplate != null)
            //        {
            //            DictionaryWellLog dictionaryWellLog = CreateDictionaryWellLog(PetrelStudioWellLog, borehole.Logs, logVersionCollection.DictionaryWellLogVersions.ToList(), borehole, dictionaryTemplate);
            //            if (dictionaryWellLog != null)
            //            {
            //                returnStatus.IsSuccess = true;
            //                returnStatus.Message = "The DictionaryWellLog '" + PetrelStudioWellLog.WellLogName + "' updated/created successfully.";
            //                returnStatusList.Add(returnStatus);
            //                PSLogger.log.Info(returnStatus.Message);
            //            }
            //            else
            //            {
            //                returnStatus.IsSuccess = false;
            //                returnStatus.Message = "The DictionaryWellLog '" + PetrelStudioWellLog.WellLogName + "' is not updated/created.";
            //                returnStatusList.Add(returnStatus);
            //                PSLogger.log.Error(returnStatus.Message);
            //            }
            //        }
            //        //If template is no null then create well log
            //        else if (template != null)
            //        {
            //            WellLog wellLog = CreateWellLog(PetrelStudioWellLog, /*borehole.Logs, logVersionCollection.WellLogVersions.ToList(), */borehole, template, logVersionCollection);

            //            if (wellLog != null)
            //            {
            //                returnStatus.IsSuccess = true;
            //                returnStatus.Message = "The LogCurve '" + PetrelStudioWellLog.WellLogName + "' updated/created successfully.";
            //                returnStatus.UpdatedValues = new Dictionary<string, string>();
            //                returnStatus.UpdatedValues.Add("GUID", wellLog.UniqueId);
            //                returnStatusList.Add(returnStatus);
            //                PSLogger.log.Info(returnStatus.Message);
            //            }
            //            else
            //            {
            //                returnStatus.IsSuccess = false;
            //                returnStatus.Message = "The LogCurve '" + PetrelStudioWellLog.WellLogName + "' is not updated/created. ";
            //                returnStatusList.Add(returnStatus);
            //                PSLogger.log.Error(returnStatus.Message);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        throw new Exception("parent well log not found");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    returnStatus.IsSuccess = false;
            //    returnStatus.Message = "Error in creating/updating WellLog: '" + PetrelStudioWellLog.WellLogName + "'. Details : " + ex.Message;
            //    returnStatusList.Add(returnStatus);
            //    PSLogger.log.Error(returnStatus.Message);
            //}
        }

        private static void CreateTemplate(string unitMeasurementName, string wellLogName, out DictionaryTemplate dictionaryTemplate, out Template template)
        {
            ITemplateService templateService = DatabaseSystem.TemplateService;
            var unitService = CoreSystem.GetService<IUnitServiceSettings>();
            dictionaryTemplate = null;
            template = null;
            IUnitMeasurement unitMeasurement = null;
            IUnit valueUnit = null;

            if (string.IsNullOrEmpty(unitMeasurementName))
            {
                unitMeasurementName = "UNKNOWN";
            }

            unitMeasurement =
                unitService.CurrentCatalog.GetUnitMeasurements()
                    .FirstOrDefault(
                        um => um.Name.Equals(unitMeasurementName, StringComparison.InvariantCultureIgnoreCase));

            if (!unitMeasurementName.Equals("UNKNOWN"))
            {
                Console.Out.WriteLine(DateTime.Now + "  -  Finding Well log templates by Mnemonics.");

                template = templateService.FindTemplateByMnemonics(RepoObject, wellLogName);

                if (template != null)
                    return;

                if (unitMeasurement != null)
                {
                    Console.Out.WriteLine(DateTime.Now + "  -  Find Well log templates by Unit Measurement. If there is only one template for the given Unit measurement, then template is returned else find template by Name.");
                    var templates = (List<Template>)templateService.FindTemplates(RepoObject, unitMeasurement);
                    string templateNames = string.Empty;
                    for (int index = 0; index < templates.Count; index++)
                    {
                        if (index == 0)
                            templateNames = templates[index].Name;
                        else
                            templateNames = templateNames + "; " + templates[index].Name;
                    }
                    if (templates.Count > 1)
                    {
                        Console.Out.WriteLine(DateTime.Now + "  -  More than one template found for the Unit Measurement: '" + unitMeasurementName + "'." + templateNames + Environment.NewLine + " Picking the first template.");
                        template = templates.First();
                    }
                    else if (templates.Count == 1)
                    {
                        Console.Out.WriteLine(DateTime.Now + "  -  Only one template found for the Unit Measurement: " + unitMeasurementName);
                        template = templates.First();
                    }
                    else
                    {
                        Console.Out.WriteLine(DateTime.Now + "  -  Zero templates found for the Unit Measurement: " + unitMeasurementName);
                    }
                }
                if (template != null)
                    return;

                template = templateService.FindTemplateByName(RepoObject, unitMeasurementName);

                if (template == null)
                {
                    TemplateType templateType;
                    if (Enum.TryParse(unitMeasurementName, out templateType))

                        template = templateService.GetAllTemplates(RepoObject).FirstOrDefault(t => t.TemplateType == templateType);
                }

                if (template != null)
                    return;

                ITemplateService service = DatabaseSystem.TemplateService;

                dictionaryTemplate = service.FindDictionaryTemplateByName(RepoObject, unitMeasurementName);

                if (dictionaryTemplate != null)
                    return;
            }

            string templateName = unitMeasurementName.Equals("UNKNOWN", StringComparison.InvariantCultureIgnoreCase) ? wellLogName : unitMeasurementName;

            template = templateService.FindTemplateByName(RepoObject, templateName);
            if (template == null)
            {
                TemplateCollection templateCollection =
                    RepoObject.TemplateCollections.FirstOrDefault(
                        tc => tc.Name.Equals("Custom Templates"));
                if (templateCollection == null)
                {
                    using (ITransaction tran = DataManager.NewTransaction())
                    {
                        tran.Lock(RepoObject);
                        templateCollection = RepoObject.CreateTemplateCollection("Custom Templates");
                        tran.Commit();
                    }
                }
                using (ITransaction transaction = DataManager.NewTransaction())
                {
                    transaction.Lock(templateCollection);
                    templateName = templateService.GetUniqueName(RepoObject, templateName);
                    if (valueUnit != null)
                    {
                        if (unitMeasurement != null)
                        {
                            transaction.Lock(templateCollection);
                            template = templateCollection.CreateTemplate(templateName, unitMeasurement);
                        }
                        else if (valueUnit.BaseMeasurement != null)
                        {
                            template = templateCollection.CreateTemplate(templateName, valueUnit.BaseMeasurement, valueUnit);
                        }
                    }
                    else if (ReferenceEquals(template, null))
                    {
                        string tempTemplateName = "UNKNOWN";

                        TemplateCollection _templateCollection = RepoObject.TemplateCollections.Where(tc =>
                        tc.Name.Equals("Custom Templates", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

                        if (ReferenceEquals(_templateCollection, null))
                            templateCollection = RepoObject.CreateTemplateCollection("Custom Templates");

                        if (!ReferenceEquals(templateCollection, null))
                            template = templateCollection.Templates.FirstOrDefault(t => t.Name.Equals("UNKNOWN"));
                        if (ReferenceEquals(template, null) && !ReferenceEquals(templateCollection, null))
                            template = templateCollection.CreateTemplate(tempTemplateName, unitMeasurement);
                    }
                    transaction.Commit();

                    IEnumerable<Exception> exceptions;
                    Console.Out.WriteLine("Start Saving Template");
                    DatabaseSystem.RepositoryService.SaveRepository(RepoObject, out exceptions);

                    foreach (var exception in exceptions)
                    {
                        Console.Out.WriteLine(exception.Message + "\n");
                    }
                    Console.Out.WriteLine("End Saving Template");
                }
            }
        }

        private static void BugWorkflow()
        {
            List<WellLogSample> samples = CreateSamples(500, 1);
            WellLog log = null;
            LogVersionCollection logVersionCollection = GetOrCreateLogVersionCollection("LogCurves");

            if (!RepoObject.TemplateCollections.Any(x => x.Name == "Custom Templates"))
                RepoObject.CreateTemplateCollection("Custom Templates");

            var unitM = CoreSystem.GetService<IUnitServiceSettings>().CurrentCatalog.GetUnitMeasurements().FirstOrDefault(um => um.Name.Equals("UNKNOWN", StringComparison.InvariantCultureIgnoreCase));
            var templateCollection = RepoObject.Queryables.AllTemplateCollections.Where(x => x.Name == "Custom Templates").FirstOrDefault();
            string templateName = "UNKNOWN";
            Template template;
            if (templateCollection.Templates.Any(x => x.Name == templateName))
                template = templateCollection.Templates.First(x => x.Name == templateName);
            else
                template = templateCollection.CreateTemplate(templateName, unitM);

            using (ITransaction trans = DataManager.NewTransaction())
            {
                WellRoot wellRoot = WellRoot.Get(RepoObject);
                trans.Lock(wellRoot);
                BoreholeCollection boreholeCollection = wellRoot.GetOrCreateBoreholeCollection();
                trans.Commit();
            }
            using (ITransaction trans = DataManager.NewTransaction())
            {
                try
                {
                   // WellWorkflow.CreateSingleBorehole();
                    DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

                    string boreholeGUID = "";// WellWorkflow.GUID; //"76986DF1970B4A9692F1945619EB4357";
                    Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(boreholeGUID);
                    Console.Out.WriteLine("Creating WellLog for Borehole {0}...", borehole.Name);
                    
                    WellLogVersion logVersion = null;
                    trans.Lock(borehole);

                    string wellLogName = "TestCurve1";
                    List<WellLogVersion> logVersions = RepoObject.Queryables.Well.AllWellLogVersions.Where(x => x.LogVersionCollection.UniqueId == logVersionCollection.UniqueId && x.Name == wellLogName).ToList();
                    while (logVersion == null)
                    {
                        logVersion = logVersions.FirstOrDefault();

                        if (logVersion == null)
                        {
                            trans.Lock(borehole.Logs);
                            logVersion = logVersionCollection.CreateWellLogVersion(wellLogName, template);
                        }

                        if (!borehole.Logs.CanCreateWellLog(logVersion))
                        {
                            logVersions.Remove(logVersion);
                            logVersion = null;
                        }
                    }

                    log = borehole.Logs.CreateWellLog(logVersion);

                    if (log.IsWritable)
                    {
                        log.Samples = samples;
                    }

                    Console.Out.WriteLine("done!");
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine("\n!!! Exception !!!\n" + ex.ToString());
                }
                finally
                {
                    trans.Commit();
                    RepositoryUtil.SaveRepository(true);
                    Console.Out.WriteLine("\nGUID: " + log.UniqueId);
                }
            }

            
        }

        public static void ExecuteWorkflow()
        {
            // CreateLogCurves();
            CreateLogCurves_v2("Well_001",0, 100);

            CreateLogCurves_v2("Well_002", 0, 100);
            /*CreateLogCurves(0, 100);
            Console.Out.WriteLine("\n100 LogCurves written in empty project. Press any key to continue...");
            Console.ReadKey();

            CreateLogCurves(100, 100);
            Console.Out.WriteLine("\n100 more LogCurves written in project. Press any key to continue...");
            Console.ReadKey();

            CreateLogCurves(0, 500);
            Console.Out.WriteLine("\n500 LogCurves written in empty project. Press any key to continue...");
            Console.ReadKey();

            CreateLogCurves(500, 500);
             Console.Out.WriteLine("\n500 more LogCurves written in project. Press any key to continue...");
            Console.ReadKey();

            CreateLogCurves(0, 1000);
            Console.Out.WriteLine("\n1000 LogCurves written in empty project. Press any key to continue...");
            Console.ReadKey();

            CreateLogCurves(1000, 1000);
            Console.Out.WriteLine("\n1000 more LogCurves written in project. Press any key to continue...");
            Console.ReadKey();*/
        }

        public static void Execute()
        {
            Console.WriteLine("\n===============================================");

            ExecuteWorkflow();

            RepositoryUtil.SaveRepository(true);
            // RepositoryUtil.OpenRepository("petrel2", "perf2");
            //BugWorkflow();
            //RepositoryUtil.CloseRepository();

            Console.WriteLine("===============================================\n");
        }
    }
}
