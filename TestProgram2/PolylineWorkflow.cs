﻿using Slb.Ocean.Geometry;
using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class PolylineWorkflow
    {
        private static Repository RepoObject => RepositoryUtil.RepoObject;
        private static string PolylineSetId { get; set; }

        private static void CreatePolylineSet(string name)
        {
            PolylineSet polylineSet = null;
            Collection collection = null;
            if (RepoObject.CollectionCount > 0 &&
                           RepoObject.Collections.Any(coll => coll.Name.Equals("SamplePolygons", StringComparison.InvariantCultureIgnoreCase)))
            {
                collection = RepoObject.Collections.Where(coll => coll.Name.Equals("SamplePolygons", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            }
            else
            {
                collection = RepoObject.CreateCollection("SamplePolygons");
            }

            polylineSet = collection.CreatePolylineSet(name);
            polylineSet.Domain = Domain.ELEVATION_DEPTH;

            PolylineSetId = polylineSet.UniqueId;
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("PolylineSet (" + name + ") successfully created and saved!\n");
        }

        private static void CreatePolyline(bool withFix = false)
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(PolylineSetId);

            List<Point3> polylinePts = new List<Point3>();
            polylinePts.Add(new Point3(497804.22, 6723541.5, -10.0));
            polylinePts.Add(new Point3(497810.34, 6723545.5, -20.0));
            polylinePts.Add(new Point3(497811.84, 6723542.5, -30.0));
            polylinePts.Add(new Point3(497804.22, 6723541.5, -10.0));

            IPolyline3 polyline3 = new Polyline3(polylinePts, true);

            IEnumerable<IPolyline3> polylines = polylineSet.Polylines;
            List<IPolyline3> polylinesList = polylines.ToList();

            if (withFix)
            {
                for (int i = 0; i < polylinesList.Count; i++)
                {
                    polylinesList[i] = new Polyline3(polylinesList[i].ToList(), polylinesList[i].IsClosed);
                }
            }

            polylinesList.Add(polyline3);
            polylineSet.Polylines = polylinesList;

            if (polylineSet.Type.Equals(PolylineType.Other))
                polylineSet.Type = PolylineType.FaultPolygons;

            Console.WriteLine("-----------------------------------------------");
            int index = 0;
            foreach (Polyline _polyline in polylineSet.Polylines)
            {
                Console.WriteLine("Polyline " + index++ + ": IsClosed = " + _polyline.IsClosed);
            }
            Console.WriteLine("-----------------------------------------------");
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("Polyline created and saved!\n");
        }

        private static void UpdateExistingPolyline(int polylineIndex)
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(PolylineSetId);

            List<Point3> polylinePts = new List<Point3>();
            polylinePts.Add(new Point3(497854.22, 6723591.5, -110.0));
            polylinePts.Add(new Point3(497860.34, 6723595.5, -120.0));
            polylinePts.Add(new Point3(497861.84, 6723592.5, -130.0));
            polylinePts.Add(new Point3(497854.22, 6723591.5, -110.0));

            IPolyline3 polyline3 = new Polyline3(polylinePts, true);

            IEnumerable<IPolyline3> polylines = polylineSet.Polylines;
            List<IPolyline3> polylinesList = polylines.ToList();

            for (int i = 0; i < polylinesList.Count; i++)
            {
                polylinesList[i] = new Polyline3(polylinesList[i].ToList(), polylinesList[i].IsClosed);
            }

            polylinesList[polylineIndex] = polyline3;
            // NullReferenceException thrown at next line
            polylineSet.Polylines = polylinesList;

            if (polylineSet.Type.Equals(PolylineType.Other))
                polylineSet.Type = PolylineType.FaultPolygons;

            Console.WriteLine("-----------------------------------------------");
            int index = 0;
            foreach (Polyline _polyline in polylineSet.Polylines)
            {
                Console.WriteLine("Polyline " + index++ + ": IsClosed = " + _polyline.IsClosed);
            }
            Console.WriteLine("-----------------------------------------------");
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("Polyline updated and saved!\n");
        }

        private static void UpdateExistingPolyline2(int polylineIndex)
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(PolylineSetId);

            List<Point3> polylinePts = new List<Point3>();
            polylinePts.Add(new Point3(497854.22, 6723591.5, -11.0));
            polylinePts.Add(new Point3(497860.34, 6723595.5, -12.0));
            polylinePts.Add(new Point3(497861.84, 6723592.5, -13.0));
            polylinePts.Add(new Point3(497854.22, 6723591.5, -11.0));
            IPolyline3 polyline3 = new Polyline3(polylinePts, true);

            IEnumerable<IPolyline3> polylines = polylineSet.Polylines;
            List<IPolyline3> polylinesList = new List<IPolyline3>();
            foreach (IPolyline3 polyline in polylines)
            {
                polylinesList.Add(polyline);
            }

            polylinesList[polylineIndex] = polyline3;
            polylineSet.Polylines = polylinesList;

            if (polylineSet.Type.Equals(PolylineType.Other))
                polylineSet.Type = PolylineType.FaultPolygons;

            Console.WriteLine("-----------------------------------------------");
            int index = 0;
            foreach (Polyline _polyline in polylineSet.Polylines)
            {
                Console.WriteLine("Polyline " + index++ + ": IsClosed = " + _polyline.IsClosed);
            }
            Console.WriteLine("-----------------------------------------------");
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("Polyline updated and saved!\n");
        }

        private static void DeleteExistingPolyline(int polylineIndex)
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(PolylineSetId);

            IEnumerable<IPolyline3> polylines = polylineSet.Polylines;
            List<IPolyline3> polylinesList = polylines.ToList();

            for (int i = 0; i < polylinesList.Count; i++)
            {
                polylinesList[i] = new Polyline3(polylinesList[i].ToList(), polylinesList[i].IsClosed);
            }

            polylinesList.RemoveAt(polylineIndex);
            polylineSet.Polylines = polylinesList;

            if (polylineSet.Type.Equals(PolylineType.Other))
                polylineSet.Type = PolylineType.FaultPolygons;

            Console.WriteLine("-----------------------------------------------");
            int index = 0;
            foreach (Polyline _polyline in polylineSet.Polylines)
            {
                Console.WriteLine("Polyline " + index++ + ": IsClosed = " + _polyline.IsClosed);
            }
            Console.WriteLine("-----------------------------------------------");
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("Polyline deleted!\n");
        }

        private static void DeleteAllPolylines()
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(PolylineSetId);

            polylineSet.Polylines = new List<IPolyline3>();

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("All polylines deleted!\n");
        }

        public static void Execute()
        {
            Console.WriteLine("\n===============================================");
            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolylineSet("PolylineSet2");
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolyline();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolyline();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolyline();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolyline();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            try
            {
                UpdateExistingPolyline(1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            try
            {
                UpdateExistingPolyline2(2);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message + "\nStackTrace: " + ex.StackTrace);
            }
            RepositoryUtil.CloseRepository();

            /*RepositoryUtil.OpenRepository("petrel2", "test_repo");
            DeleteExistingPolyline(4);
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            DeleteExistingPolyline(2);
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            DeleteAllPolylines();
            RepositoryUtil.CloseRepository();*/

            /*RepositoryUtil.OpenRepository("petrel2", "test_repo");
            RenamePolylineSet("1BEF2875375845C0A7CCC70D0DFCFF4E", "nam_horizon_01_orig");
            RepositoryUtil.CloseRepository();*/

            /*RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolylineSetForDemo("nam_horizon_01");
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolylineSetForDemo("nam_horizon_02");
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreatePolylineSetForDemo("nam_horizon_03");
            RepositoryUtil.CloseRepository();*/

        }

        private static void CreatePolylineSetForDemo(string name)
        {
            Collection collection = RepoObject.Collections.Where(coll => coll.Name.Equals("Polygon Collection", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

            PolylineSet polylineSet = collection.CreatePolylineSet(name);
            polylineSet.Domain = Domain.ELEVATION_TIME;
            double datum = 200;
            polylineSet.SrdReference = GetOrCreateSrdReference(datum, name);
            polylineSet.Type = PolylineType.FaultPolygons;
            ITypedFeature<string> p = polylineSet.Extension.CreateAttribute<string>("CustomAttributes", "data_source");
            p.Value = "01STA";
            p = polylineSet.Extension.CreateAttribute<string>("CustomAttributes", "IsMappingPolygon");
            p.Value = "true";

            PolylineSetId = polylineSet.UniqueId;

            string existingPolylineSetId = "1BEF2875375845C0A7CCC70D0DFCFF4E";
            PolylineSet existingPolylineSet = RepoObject.ResolveUniqueId<PolylineSet>(existingPolylineSetId);
            IEnumerable<IPolyline3> polylines = existingPolylineSet.Polylines;
            List<IPolyline3> polylinesList = polylines.ToList();
            for (int i = 0; i < polylinesList.Count; i++)
            {
                var pointsList = polylinesList[i].ToList();
                for (int j = 0; j < pointsList.Count; j++)
                {
                    pointsList[j] = new Point3(pointsList[j].X + 123, pointsList[j].Y + 123, pointsList[j].Z - 123);
                }
                polylinesList[i] = new Polyline3(pointsList, polylinesList[i].IsClosed);
            }
            polylineSet.Polylines = polylinesList;

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("PolylineSet (" + name + ") successfully created and saved!\n");
        }

        private static SrdReference GetOrCreateSrdReference(double datum, string name, double replacementVelocity = 1480)
        {
            var list = RepoObject.GetSrdReferences();
            foreach (SrdReference srd in list)
            {
                if (decimal.Round((decimal)datum, 2) == decimal.Round((decimal)srd.ElevationToTimeZero, 2))
                    return srd;
            }
            return RepoObject.GetOrCreateSrdReference(name, datum, replacementVelocity);
        }

        private static void RenamePolylineSet(string id, string newName)
        {
            PolylineSet polylineSet = RepoObject.ResolveUniqueId<PolylineSet>(id);

            string oldName = polylineSet.Name;
            polylineSet.Name = newName;

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            Console.WriteLine("Renamed PolylineSet from '" + oldName + "' to '" + newName + "'.\n");
        }

    }
}