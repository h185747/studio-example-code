﻿using Slb.Ocean.Studio.Data.Petrel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class RepositoryUtil : IDisposable
    {
        public static Repository RepoObject { get; set; }
        DatabaseSystem dbSystem = new DatabaseSystem();
        public static DataConnectionContext dataConnectionContext { get; set; }

        public static bool InitializeSdk()
        {
            try
            {
                DomainObjectSetup.Initialize();
            }
            catch (Exception ex1)
            {
                try
                {
                    DomainObjectSetup.Initialize(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
                }
                catch (Exception ex2)
                {
                    Console.WriteLine("Failed to initialize SK SDK.");
                    Console.WriteLine(ex1.Message);
                    Console.WriteLine(ex2.Message);
                    return false;
                }
            }
            return true;
        }

        public static void OpenRepository(string dbInstance, string repository, bool printInfo = false)
        {
            InitializeSdk();

            
            
            DatabaseSystem.Initialize();
            if (printInfo)
                Console.WriteLine("Connecting to studio server database...");
            dataConnectionContext = DataConnectionContext.Create(dbInstance);
           // Console.ReadKey();

            if (printInfo)
            {
                Console.WriteLine("Connected to studio server version: " + DatabaseSystem.GetStudioServerVersion(dataConnectionContext));
                //Console.ReadKey();
            }

                if (DatabaseSystem.IsVersionCompatible(dataConnectionContext))
            {
                if (printInfo)
                    Console.WriteLine("Synchronizing coordinate catalog...");

                DatabaseSystem.CoordinateService.SynchronizeCoordinateCatalog(dataConnectionContext);
               // Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Sdk version is not compatible to studio server");
                throw new Exception(string.Format("Sdk version is not compatible to studio server database: {0}", dbInstance));
            }

            if (printInfo)
                Console.WriteLine(string.Format("Opening Repository: {0}", repository));
            RepoObject = DatabaseSystem.RepositoryService.OpenRepository(dataConnectionContext, repository);
            if (printInfo)
                Console.WriteLine(string.Format("Repository opened: {0}", repository));
        }

        public static void CloseRepository()
        {
            DatabaseSystem.RepositoryService.CloseRepository(RepoObject);
            DatabaseSystem.RepositoryService.Dispose();
            //dataConnectionContext.Dispose();            
            GC.Collect();
            RepoObject = null;
        }

        public static void SaveRepository(bool printInfo = false)
        {
            if (printInfo)
                Console.Out.Write("Saving Repository... ");
            DateTime start = DateTime.Now;
           DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            double time = (DateTime.Now - start).TotalSeconds;
            if (printInfo)
                Console.Out.WriteLine("done! (Save took {0}s)", time);
        }

        public void Dispose()
        {
            DatabaseSystem.RepositoryService.Dispose();
            dbSystem.Dispose();
            GC.Collect();

            //throw new NotImplementedException();
        }
    }
}