﻿using Slb.Ocean.Basics;
using Slb.Ocean.Coordinates;
using Slb.Ocean.Core;
using Slb.Ocean.Geometry;
using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.IO;
using Slb.Ocean.Studio.Data.Petrel.Seismic;
using Slb.Ocean.Studio.Data.Petrel.UI;
using Slb.Ocean.Studio.Data.Petrel.Well;
using Slb.Pdb.Sdk.Admin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class SeismicWorkflow
    {
        private static int startLine= 8267;
        private static int startTrace= 4567;
        private static int endSample= 1500;
        private static int startSample=0;
        private static int endTrace=7597;
        private static int endLine= 9823;
        private static int buffer_size= (1024 * 1024 * 50) / 4;

        static List<float[]>  flList = new List<float[]>();
        private static int lastline = -1;
        private static int lasttrace = -1;
        private static double TotalTimeAccumulated;

        private static Repository RepoObject => RepositoryUtil.RepoObject;
        private static string GUID { get; set; }
        public class SeismicChunk
        {
            private int fromLine;
            private int fromTrace;
            private int tracesToRead;

            public SeismicChunk()
            {
            }

            public int FromLine
            {
                get { return fromLine; }
                set { fromLine = value; }
            }

            public int FromTrace
            {
                get { return fromTrace; }
                set { fromTrace = value; }
            }

            public int TracesToRead
            {
                get { return tracesToRead; }
                set { tracesToRead = value; }
            }
        }

        public enum QueryParams
        {
            //START_TRACE = "x0",
            //NUM_TRACES = "nx",
            //END_TRACE = "ex",
            //START_SAMPLE = "s0",
            //NUM_SAMPLES = "ns",
            //END_SAMPLE = "es",
            //START_LINE = "y0",
            //END_LINE = "ey",
            //NUM_LINES = "ny",
            //MODE = "mode"
        }


        //private static Seismic3DCollection GetOrCreateSeismic3DCollection(string name = "Seismic Data")
        //{
        //    Seismic3DCollection s3dc = RepoObject.Queryables.Seismic.AllSeismic3DCollections.Where(x => x.Name == name).FirstOrDefault();
        //    if (s3dc == null)
        //    {
        //        var seismicProject = SeismicRoot.Get(RepoObject).GetOrCreateSeismicProject();
        //        s3dc = (Seismic3DCollection)(seismicProject.CreateSeismicCollection(name, typeof(SeismicCube)));
        //    }
        //    return s3dc;
        //}
        //static List<int> GetSeismicCubeDataInfo(string GUID, int startInline, int endInline, int startCrossline, int endCrossline, Repository repository)
        //{
        //    //IndexDouble3 startAnnotation = new IndexDouble3(startInline, startCrossline, 0);
        //    //IndexDouble3 endAnnotation = new IndexDouble3(endInline, endCrossline, 0);

        //    List<int> list = new List<int>();
        //    SeismicCube cube = GetSeismicCubeDomainObject(GUID, repository);
        //    if (cube == null)
        //    {
        //        return null;
        //    }

        //    double[] inline = { startInline, endInline };
        //    double[] crossLine = { startCrossline, endCrossline };
        //    float inlineSpacing = cube.SpatialLattice.OriginalLattice.Single.SpacingInline;
        //    float crosslineSpacing = cube.SpatialLattice.OriginalLattice.Single.SpacingCrossline;
        //    //ITrace startIndex = cube.Traces.FirstOrDefault(); // startAnnotation; // cube.IndexAtAnnotation(startAnnotation);
        //    //ITrace endIndex = cube.Traces.LastOrDefault();    //endAnnotation; //cube.IndexAtAnnotation(endAnnotation);
        //    cube.SpatialLattice.OriginalLattice.Operations.AnnotToIndex(inline, crossLine);                             // applying ceil and floor to calculate the range.

        //    list.Add(resolveIndex(inline[0], true, inlineSpacing));
        //    list.Add(resolveIndex(inline[1], false, inlineSpacing));
        //    list.Add(resolveIndex(crossLine[0], true, crosslineSpacing));
        //    list.Add(resolveIndex(crossLine[1], false, crosslineSpacing));
        //    list.Add(Convert.ToInt32(inlineSpacing));
        //    list.Add(Convert.ToInt32(crosslineSpacing));
        //    return l

        //private static SpatialLatticeInfo CreateSpatialLatticeInfo()
        //{
        //    double[,] points = new double[4, 2];
        //    points[0, 0] = 451199.854599822;
        //    points[0, 1] = 6780362.88986858;
        //    points[1, 0] = 451330.240449782;
        //    points[1, 1] = 6789694.64978386;
        //    points[2, 0] = 458699.552043353;
        //    points[2, 1] = 6780258.10208623;

        //    ICoordinateOperation crsOperation;
        //    CoreSystem.GetService<ICoordinateService>().CreateOperation(RepoObject.CoordinateReferenceSystem, RepoObject.CoordinateReferenceSystem, PrePostUnitConversions.ToSI, out crsOperation);
        //    crsOperation.Convert(ref points);

        //    // The world points for the lattice are expected in SI (meter)
        //    LatticeInfo lattice = new LatticeInfo(375, 301,
        //                                          points[0, 0], points[0, 1],
        //                                          points[1, 0], points[1, 1],
        //                                          points[2, 0], points[2, 1],
        //                                          152, 150,
        //                                          900, 750,
        //                                          true);
        //    return new SpatialLatticeInfo(lattice, RepoObject.CoordinateReferenceSystem);
        //}

        //public static void CreateSeismicCubes(int num = 1000)
        //{
        //    var seismicProject = SeismicRoot.Get(RepoObject).GetOrCreateSeismicProject();
        //    Seismic3DCollection s3dc = GetOrCreateSeismic3DCollection();
        //    SeismicCollection sc = (SeismicCollection)s3dc;
        //    sc.SpatialLattice = CreateSpatialLatticeInfo();
        //    ICoordinateReferenceSystem crs = s3dc.OriginalCRS;
        //    Template seismicTemplate = DatabaseSystem.TemplateService.FindTemplateByName(RepoObject, "Seismic (default)");

        //    string zgyCropFileName = "D:\\Users\\hasham.anwar\\source\\repos\\TestProgram2\\TestProgram2\\Orig Amp Crop.zgy";
        //    SeismicCube cropCube = CoreSystem.GetService<IZgyFormat>().ImportSeismic3D(zgyCropFileName, sc,
        //                                                        seismicProject.CreateVintage("Seismic Time Orig - 1"),
        //                                                        Domain.ELEVATION_TIME, seismicTemplate,
        //                                                        sc.SpatialLattice.OriginalCRS);
        //    cropCube.Name = "Imported Orig Amp Crop";
        //    cropCube.SrdReference = RepoObject.GetOrCreateSrdReference("Western Desert", 300, 2100);

        //    for (int index = 0; index < num; index++)
        //    {
        //        string id = index.ToString().PadLeft(4, '0');
        //        string name = "Orig Amp Crop - " + id;
        //        Console.Out.Write("Creating SeismicCube {0}...", name);
        //        SeismicCube cropCube2 = sc.CreateSeismicCube(cropCube.SpatialLattice, cropCube.Origin.Z, cropCube.NumSamplesIJK.K, cropCube.SampleSpacingIJK.Z, typeof(sbyte),
        //                                    Domain.ELEVATION_TIME, seismicTemplate, new Range1<double>(cropCube.ClippingRange.Min, cropCube.ClippingRange.Max), null);
        //        cropCube2.Vintage = seismicProject.CreateVintage("Seismic Time - " + id);
        //        cropCube2.Name = name;
        //        cropCube2.SrdReference = RepoObject.GetOrCreateSrdReference("Western Desert", 300, 2100);
        //        ITypedFeature<string> versionNameFeature = cropCube2.Extension.CreateProperty<string>("seismic_version_name");
        //        versionNameFeature.Value = "v" + id;
        //        try
        //        {
        //            int numI = cropCube2.NumSamplesIJK.I;
        //            int numJ = cropCube2.NumSamplesIJK.J;
        //            int numK = cropCube2.NumSamplesIJK.K;
        //            if (cropCube2.IsWritable)
        //            {
        //                for (int i = 0; i < numI; i++)
        //                {
        //                    for (int j = 0; j < numJ; j++)
        //                    {
        //                        float[] buffer = new float[numK];
        //                        cropCube.GetTraceData(i, j, buffer);

        //                        cropCube2.SetTraceData(i, j, buffer);
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Console.Out.WriteLine("Error writing Seismic 3D Trace: " + e.Message);
        //            Console.Out.WriteLine(e.StackTrace);
        //        }
        //        Console.Out.Write("done! ...  ");
        //        RepositoryUtil.SaveRepository(true);
        //    }

        //    Console.Out.Write("Deleting cropCube...");
        //    cropCube.Delete();
        //    RepositoryUtil.SaveRepository(true);
        //}
        public static List<SeismicChunk> GetSeismicChunks()
        {
            List<SeismicChunk> list = new List<SeismicChunk>();
            int currentLine = 0;// startLine,
            int currentTrace = 0;
            SeismicChunk chunk;
            int sampleArraySize = 1501;// endSample - startSample + 1;
            int traceCount = 3031;
            int lineCount =1557;
            int totalTraces = traceCount * lineCount;
            int tracesPerReq = buffer_size / sampleArraySize;
            int lineCounter = tracesPerReq / traceCount;
            int traceCounter = tracesPerReq % traceCount;
            int reqCounter = 0;

            while (reqCounter < totalTraces)
            {
                chunk = new SeismicChunk();
                chunk.FromLine = currentLine;
                chunk.FromTrace = currentTrace;

                if (reqCounter + tracesPerReq < totalTraces)
                {
                    chunk.TracesToRead = tracesPerReq;
                }
                else
                {
                    chunk.TracesToRead = totalTraces - reqCounter;
                }

                currentLine += lineCounter;
                currentTrace += traceCounter;

                if (currentTrace > endTrace)
                {
                    currentLine++;
                    currentTrace = startTrace + (currentTrace - endTrace - 1);
                }

                reqCounter += tracesPerReq;
                list.Add(chunk);
            }

            return list;
        }


        public static void Execute()
        {
            Console.WriteLine("\n===============================================");

            RepositoryUtil.OpenRepository("20", "equinor_issue");

            string folderPath = Environment.GetEnvironmentVariable("STUDIO_SEGY_PATH");
            RepoObject.ExternalFilesFolderPath = folderPath;
            var chunkList = GetSeismicChunks();
            SeismicChunk chunk = chunkList[0];
            //foreach (SeismicChunk chunk in chunkList)
            //{
                readOldSeismicCube("364A7EA10FA748D2AF28B6F07086B325", 0, 1556, 0, 3030, getFromLine(chunk.FromLine),
                            getFromTrace(chunk.FromTrace, chunk.FromLine), chunk.TracesToRead, 0, 1500, RepositoryUtil.RepoObject, folderPath);
                readNewSeismicCube("364A7EA10FA748D2AF28B6F07086B325", 0, 1556, 0, 3030, getFromLine(chunk.FromLine),
                            getFromTrace(chunk.FromTrace, chunk.FromLine), chunk.TracesToRead, 0, 1500, RepositoryUtil.RepoObject, folderPath);

            //}
            RepositoryUtil.CloseRepository();

            Console.WriteLine("===============================================\n");
        }

        private static int getFromTrace(int fromTrace, int fromLine)
        {
            if (fromLine == lastline)
                return lasttrace + 1;
            else return 0;
        }

        private static int getFromLine(int fromLine)
        {
            return fromLine;
        }

        private static SeismicCube GetSeismicCubeDomainObject(string GUID, Repository repository)
        {
            if (!String.IsNullOrEmpty(GUID))
            {
                SeismicCube cube = repository.Queryables.Seismic.AllSeismicCubes.Where(x => x.UniqueId == GUID).FirstOrDefault();

                return cube;
            }
            return null;
        }

        private static void readNewSeismicCube
           (string GUID, int startLine, int endLine, int startTrace, int endTrace, int fromLine, int fromTrace, int numberOfTracesToRead, int startSample,
           int endSample, Repository repository, String folderPath)
        {
            DateTime start = DateTime.Now;
            SeismicProject sc_project = SeismicRoot.Get(repository).GetOrCreateSeismicProject();
            
            repository.ExternalFilesFolderPath = folderPath;

            SeismicCube cube = GetSeismicCubeDomainObject(GUID, repository);

            // cube.ReferenceFileName = folderPath + "\\" + Guid.Parse(cube.UniqueId) + ".zgy";
            int sampleArraySize = endSample - startSample + 1;
            // if required samplesInTrace size exceeds the original size, correct it.
            if (cube.NumSamplesIJK.K < sampleArraySize)
            {
                sampleArraySize = cube.NumSamplesIJK.K;
            }

            int tracesRead = 0;
            int currentTrace = fromTrace, currentLine = fromLine;


           



            //Array3FBufList array3FListBuilder = new Array3FBufList();
            List<float[]> floatArrays = new List<float[]>();
            List<Index2> index2Arrays = new List<Index2>();
            List<double> XCum = new List<double>();
            List<double> YCum = new List<double>();
            DateTime start2 = DateTime.Now;
            while (tracesRead != numberOfTracesToRead)
            {
                index2Arrays.Add(new Index2(currentLine, currentTrace));
                float[] traceData = new float[cube.NumSamplesIJK.K];
                double[] x_array = { currentLine };
                double[] y_array = { currentTrace };
                XCum.Add(x_array[0]);
                YCum.Add(y_array[0]);
                //double[] x_array = recvTraceList.Select(xx => xx.Length.I).ToArray().Select(Convert.ToDouble).ToArray();
                //double[] y_array = recvTraceList.Select(xx => xx.Length.J).ToArray().Select(Convert.ToDouble).ToArray();

                cube.SpatialLattice.OriginalLattice.Operations.IndexToAnnot(x_array, y_array);
                floatArrays.Add(traceData);
                tracesRead++;
                currentTrace++;
                if (currentTrace > endTrace)
                {
                    currentLine++;
                    if (currentLine > endLine)
                    {
                        break;
                    }
                    currentTrace = startTrace;
                }
            }
            
            tracesRead = 0;
            currentTrace = fromTrace; currentLine = fromLine;
            DateTime start1 = DateTime.Now;
            cube.GetTracesData(index2Arrays, floatArrays);
            
            int index = 0;
            DateTime start3 = DateTime.Now;
            while (tracesRead != numberOfTracesToRead)
            {
                
                var traceData = floatArrays[index];

                // TODO: compare the actual k values instead of size.
                if (sampleArraySize == cube.NumSamplesIJK.K)
                {
                    flList.Add(traceData);
                }
                else
                {
                    // TODO: rewrite.
                    float[] truncatedTrace = new float[sampleArraySize];
                    Array.Copy(traceData, startSample, truncatedTrace, 0, sampleArraySize);
                    flList.Add(truncatedTrace);
                }


                // Point3 point = cube.PositionAtIndex(new IndexDouble3(currentLine, currentTrace, startSample));
                Point2 point;
                if (cube.SpatialLattice.OriginalCRS != null)
                    point = cube.SpatialLattice.PositionAtIndex(new IndexDouble2(currentLine, currentTrace), cube.SpatialLattice.OriginalCRS);
                else
                    point = cube.SpatialLattice.PositionAtIndex(new IndexDouble2(currentLine, currentTrace), repository.CoordinateReferenceSystem);

                

                tracesRead++;
                currentTrace++;
                if (currentTrace > endTrace)
                {
                    currentLine++;
                    if (currentLine > endLine)
                    {
                        break;
                    }
                    currentTrace = startTrace;
                }
                index++;
                if (index > floatArrays.Count)
                    break;
                if ((flList.Count() * cube.NumSamplesIJK.K * 4) / (1024.0 * 1024.0 * 1024.0) > 1)
                    break;
            }

         

            
            DateTime end = DateTime.Now;
            double preciseDifference = (end - start).TotalSeconds;
            TotalTimeAccumulated = TotalTimeAccumulated + preciseDifference;
            Console.WriteLine("get seismic traces :" + preciseDifference + " traces read " + numberOfTracesToRead + " in single call");
            Console.WriteLine("Total Time Accumulated :" + TotalTimeAccumulated);
            Console.WriteLine("Size accumalated is " + (flList.Count() * cube.NumSamplesIJK.K * 4) / (1024.0 * 1024.0) + " MB");
            lastline = currentLine;
            lasttrace = currentTrace;


        }
        private static void readOldSeismicCube
            (string GUID, int startLine, int endLine, int startTrace, int endTrace, int fromLine, int fromTrace, int numberOfTracesToRead, int startSample, 
            int endSample, Repository repository, String folderPath)
        {
            DateTime start = DateTime.Now;
            SeismicProject sc_project = SeismicRoot.Get(repository).GetOrCreateSeismicProject();
           
            repository.ExternalFilesFolderPath = folderPath;

            SeismicCube cube = GetSeismicCubeDomainObject(GUID, repository);

            // cube.ReferenceFileName = folderPath + "\\" + Guid.Parse(cube.UniqueId) + ".zgy";
            int sampleArraySize = endSample - startSample + 1;
            // if required samplesInTrace size exceeds the original size, correct it.
            if (cube.NumSamplesIJK.K < sampleArraySize)
            {
                sampleArraySize = cube.NumSamplesIJK.K;
            }

            int tracesRead = 0;
            int currentTrace = fromTrace, currentLine = fromLine;


            while (tracesRead != numberOfTracesToRead)
            {
                float[] traceData = new float[cube.NumSamplesIJK.K];
                //Console.WriteLine("line " + currentLine + " trace " + currentTrace);
                cube.GetTraceData(currentLine, currentTrace, traceData);
              
                // TODO: compare the actual k values instead of size.
                if (sampleArraySize == cube.NumSamplesIJK.K)
                {
                    flList.Add(traceData);
                }
                else
                {
                    // TODO: rewrite.
                    float[] truncatedTrace = new float[sampleArraySize];
                    Array.Copy(traceData, startSample, truncatedTrace, 0, sampleArraySize);
                    flList.Add(truncatedTrace);
                }

                
                //IndexDouble3 annotation = new IndexDouble3(currentLine, currentTrace, sampleArraySize); // cube.AnnotationAtIndex(new IndexDouble3(currentLine, currentTrace, sampleArraySize));
                double[] x_array = { currentLine };
                double[] y_array = { currentTrace };

                //double[] x_array = recvTraceList.Select(xx => xx.Length.I).ToArray().Select(Convert.ToDouble).ToArray();
                //double[] y_array = recvTraceList.Select(xx => xx.Length.J).ToArray().Select(Convert.ToDouble).ToArray();

                cube.SpatialLattice.OriginalLattice.Operations.IndexToAnnot(x_array, y_array);
               

                
                // Point3 point = cube.PositionAtIndex(new IndexDouble3(currentLine, currentTrace, startSample));
                Point2 point = cube.SpatialLattice.PositionAtIndex(new IndexDouble2(currentLine, currentTrace), repository.CoordinateReferenceSystem);


                tracesRead++;
                currentTrace++;
                if (currentTrace > endTrace)
                {
                    currentLine++;
                    if (currentLine > endLine)
                    {
                        break;
                    }
                    currentTrace = startTrace;
                }
                if ((flList.Count() * cube.NumSamplesIJK.K * 4) / (1024.0 * 1024.0 * 1024.0)>1)
                    break;

            }
            DateTime end = DateTime.Now;
            double preciseDifference = (end - start).TotalSeconds;
            TotalTimeAccumulated = TotalTimeAccumulated + preciseDifference;
            Console.WriteLine("get seismic traces :" + preciseDifference+" traces read "+ numberOfTracesToRead+" in "+ numberOfTracesToRead+" calls");
            Console.WriteLine("Total Time Accumulated :" + TotalTimeAccumulated);
            Console.WriteLine("Size accumalated is " + (flList.Count() * cube.NumSamplesIJK.K * 4) / (1024.0 * 1024.0)+" MB");
            lastline = currentLine; 
            lasttrace = currentTrace;
        }


    }
}
