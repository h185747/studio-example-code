﻿using Slb.Ocean.Core;
using Slb.Ocean.Geometry;
using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.UI;
using Slb.Ocean.Studio.Data.Petrel.Well;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestProgram2
{
    class ConnectionTests
    {
        public static string GUID { get; set; }

        public static void Execute()
        {
            Console.WriteLine("\n===============================================");

            RepoManagementPOC();

            Console.WriteLine("===============================================\n");
        }

        public static void RepoManagementPOC()
        {
            RepositoryUtil.InitializeSdk();

            string wellid = "";// WellWorkflow.GUID;

            ConnectionTests tests = new ConnectionTests();
            
            Thread thread1 = new Thread(new ThreadStart(() => tests.DeleteWell("repo1", wellid)));
            Thread thread2 = new Thread(new ThreadStart(() => tests.SelectWell("repo1", wellid)));
            
            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
            Thread.Sleep(300000);

        }

        public void DeleteWell(string repository, string wellid)
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine("Thread[{0}] - started", id);

            DataConnectionContext dataConnectionContext;
            DatabaseSystem.Initialize();
            Console.WriteLine("Connecting to studio server database...");
            dataConnectionContext = DataConnectionContext.Create("MSSQLSERVER_20");
            Console.WriteLine("Connected to studio server version: " + DatabaseSystem.GetStudioServerVersion(dataConnectionContext));
            if (DatabaseSystem.IsVersionCompatible(dataConnectionContext))
            {
                Console.WriteLine("Synchronizing coordinate catalog...\n");
                DatabaseSystem.CoordinateService.SynchronizeCoordinateCatalog(dataConnectionContext);
            }
            else
            {
                Console.WriteLine("Sdk version is not compatible to studio server");
                throw new Exception(string.Format("Sdk version is not compatible to studio server database: {0}", "studio20"));
            }

            Repository repoObject1;

            Console.WriteLine(string.Format("Thread[{0}] - Opening Repository: {1}", id, repository));
            repoObject1 = DatabaseSystem.RepositoryService.OpenRepository(dataConnectionContext, repository);
            Console.WriteLine(string.Format("Thread[{0}] - Repository opened: {1}", id, repository));

            Console.WriteLine("Thread[{0}] - Sleeping for 10s.", id);
            Thread.Sleep(5000);
            Console.WriteLine("Thread[{0}] - Resuming", id);


            Borehole borehole = repoObject1.ResolveUniqueId<Borehole>(wellid);
            using (ITransaction transaction = DataManager.NewTransaction())
            {
                transaction.Lock();
                Console.WriteLine("Thread[{0}] - Deleting borehole.", id);
                borehole.Delete();
                transaction.Commit();
            }
            DatabaseSystem.RepositoryService.SaveRepository(repoObject1);
            DatabaseSystem.RepositoryService.ClearCache(repoObject1);

            Console.WriteLine("Thread[{0}] - Borehole deleted.", id);

            Console.WriteLine("Thread[{0}] - Sleeping for 180s.", id);
            Thread.Sleep(180000);
            Console.WriteLine("Thread[{0}] - Resuming and closing", id);

            if (repoObject1 != null)
                DatabaseSystem.RepositoryService.CloseRepository(repoObject1);
            Console.WriteLine("Thread[{0}] - completed", id);
        }

        public void SelectWell(string repository, string wellid)
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            Console.WriteLine("Thread[{0}] - started", id);

            DataConnectionContext dataConnectionContext;
            DatabaseSystem.Initialize();
            Console.WriteLine("Connecting to studio server database...");
            dataConnectionContext = DataConnectionContext.Create("MSSQLSERVER_20");
            Console.WriteLine("Connected to studio server version: " + DatabaseSystem.GetStudioServerVersion(dataConnectionContext));
            if (DatabaseSystem.IsVersionCompatible(dataConnectionContext))
            {
                Console.WriteLine("Synchronizing coordinate catalog...\n");
                DatabaseSystem.CoordinateService.SynchronizeCoordinateCatalog(dataConnectionContext);
            }
            else
            {
                Console.WriteLine("Sdk version is not compatible to studio server");
                throw new Exception(string.Format("Sdk version is not compatible to studio server database: {0}", "studio20"));
            }

            Repository repoObject2;

            Console.WriteLine(string.Format("Thread[{0}] - Opening Repository: {1}", id, repository));
            repoObject2 = DatabaseSystem.RepositoryService.OpenRepository(dataConnectionContext, repository);
            Console.WriteLine(string.Format("Thread[{0}] - Repository opened: {1}", id, repository));

            Borehole borehole = repoObject2.ResolveUniqueId<Borehole>(wellid);
            Console.WriteLine(string.Format("Thread[{0}] - Borehole({1}) resolved", id, borehole.UniqueId));

            Console.WriteLine("Thread[{0}] - Sleeping for 60s.", id);
            Thread.Sleep(60000);
            Console.WriteLine("Thread[{0}] - Resuming", id);


            Borehole borehole2 = repoObject2.ResolveUniqueId<Borehole>(wellid);
            if (borehole2 == null)
                Console.WriteLine("Borehole not found using 'ResolveUniqueId'");
            else
                Console.WriteLine("Borehole found using 'ResolveUniqueId'");

            Borehole borehole3 = repoObject2.Queryables.Well.AllBoreholes.FirstOrDefault(x => x.UniqueId == wellid);
            if (borehole3 == null)
                Console.WriteLine("Borehole not found using 'Queryables'");
            else
                Console.WriteLine("Borehole found using 'Queryables'");

            if (repoObject2 != null)
                DatabaseSystem.RepositoryService.CloseRepository(repoObject2);
            Console.WriteLine("Thread[{0}] - completed", id);
            Console.WriteLine("Thread[{0}] - Closing", id);
        }

    }
}
