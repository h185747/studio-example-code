﻿using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using Slb.Ocean.Studio.Data.Petrel.Seismic;
using Slb.Ocean.Units;
using Slb.Ocean.Geometry;

namespace TestProgram2
{
    class PointSetWorkflow
    {
        private static Repository RepoObject => RepositoryUtil.RepoObject;

        private static int totalPoints { get; set; } = 100000000;

        private static string CreateNewPointSet(string name = null)
        {
            string guid = "";
            if (string.IsNullOrEmpty(name))
                name = "PointSet_" + new Random().Next(1, 100000);
            RepositoryUtil.OpenRepository("petrel2", "test_repo");

            var collection = RepoObject.Collections.Where(x => x.Name.Equals("PointSet Collection", StringComparison.InvariantCultureIgnoreCase)).First();
            PointSet pointSet = collection.CreatePointSet(name);

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);
            guid = pointSet.UniqueId; 
            Console.WriteLine("\nPointSet {0} saved. GUID: {1}\n", name, guid);

            RepositoryUtil.CloseRepository();
            return guid;
        }

        private static void ClearPointSetData(string guid)
        {
            RepositoryUtil.OpenRepository("petrel2", "test_repo");

            PointSet pointSet = RepoObject.ResolveUniqueId<PointSet>(guid);

            int oldCount = pointSet.Points != null ? pointSet.Points.Count() : 0;
            List<Point3> points = new List<Point3>();
            pointSet.Points = new Point3Set(points);
            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

            int newCount = pointSet.Points != null ? pointSet.Points.Count() : 0;
            Console.WriteLine("\nPointSet updated. Points count " + oldCount + " -> " + newCount + "\n");

            RepositoryUtil.CloseRepository();
        }

        private static List<Point3> PrepareData(int chunk, int totalChunks)
        {
            int chunkSize = totalPoints / totalChunks;
            Console.WriteLine("\nPreparing {0} points for chunk {1}/{2}...", chunkSize, chunk+1, totalChunks);
            DateTime dateTime1 = DateTime.Now;
            List<Point3> points = new List<Point3>();
            double xy_inc = 0.0001;
            double z_inc = 0.0001;
            double x = 406372.5 + chunk * xy_inc * chunkSize;
            double y = 7244531.5 + chunk * xy_inc * chunkSize;
            double z = 0 + chunk * z_inc * chunkSize;
            for (int i = 0; i < chunkSize; i++)
            {
                points.Add(new Point3(x, y, z));
                x += xy_inc;
                y += xy_inc;
                z += z_inc;
            }
            DateTime dateTime2 = DateTime.Now;
            var diff = (dateTime2 - dateTime1).TotalSeconds;
            Console.WriteLine("*** Data prep took {0}s ***\n", diff);
            return points;
        }

        private static void AddPointSetDataInChunks(string guid, int nChunks = 1)
        {
            double totalTime = 0;

            for (int i = 0; i < nChunks; i++)
            {
                List<Point3> points = PrepareData(i, nChunks);

                DateTime dateTime1 = DateTime.Now;

                RepositoryUtil.OpenRepository("petrel2", "test_repo");

                AddData(guid, points, i == 0);

                RepositoryUtil.CloseRepository();

                DateTime dateTime2 = DateTime.Now;
                totalTime += (dateTime2 - dateTime1).TotalSeconds;
            }

            Console.WriteLine("\n*** Data write in {0} chunks took {1}s ***\n", nChunks, totalTime);
        }

        private static void AddData(string guid, List<Point3> pointsToAdd, bool overwritePoints = false)
        {
            PointSet pointSet = RepoObject.ResolveUniqueId<PointSet>(guid);

            int oldCount = pointSet.Points != null ? pointSet.Points.Count() : 0;

            if (overwritePoints)
            {
                pointSet.Points = new Point3Set(pointsToAdd);
            }
            else
            {
                List<Point3> pointList = pointSet.Points.ToList();
                pointList.AddRange(pointsToAdd);
                pointSet.Points = new Point3Set(pointList);
            }

            IEnumerable<Exception> exceptions;
            var x = DatabaseSystem.RepositoryService.SaveRepository(RepoObject, out exceptions);
            foreach (Exception ex in exceptions)
                Console.WriteLine(ex.Message + "\n" + ex.ToString());

            int newCount = pointSet.Points != null ? pointSet.Points.Count() : 0;
            Console.WriteLine("  AddData: PointSet saved. Points count " + oldCount + " -> " + newCount);
        }

        private static void RunWorkflow(int totalChunks)
        {
            string guid = "";
            List<int> totalPointsList = new List<int>(){ 80000000, 50000000, 10000000, 1000000, 100000, 1000 };
            foreach (int npoints in totalPointsList)
            {
                totalPoints = npoints;
                string name = "Pointset_";
                if (npoints < 1000000)
                    name += npoints;
                else
                    name += (npoints / 1000000 + "mill");
                guid = CreateNewPointSet(name);
                AddPointSetDataInChunks(guid, totalChunks);
                //ClearPointSetData(guid);
            }
        }

        private static void ReadPointSet(string guid)
        {
            time1 = 0;
            time2 = 0;
            DateTime start = DateTime.Now;

            RepositoryUtil.OpenRepository("petrel2", "test_repo");

            PointSet pointSet = RepoObject.ResolveUniqueId<PointSet>(guid);
            int totalPoints = pointSet.Points != null ? pointSet.Points.Count() : 0;
            Console.WriteLine("\nReading PointSet '" + pointSet.Name + "'. Points count: " + totalPoints + "\n");

            RepositoryUtil.CloseRepository();

            int fromIndex = 0;
            int toIndex = -1;
            int chunkSize = (1024 * 1024 * 10) / (8 + 8 + 4);
            do
            {
                fromIndex = toIndex + 1;
                toIndex = fromIndex + chunkSize - 1;

                if (toIndex > totalPoints - 1)
                    toIndex = totalPoints - 1;

                ReadInChunks(guid, fromIndex, toIndex);


            } while (toIndex < totalPoints - 1);

            Console.WriteLine("Total Read time: {0}s.   Time1: {1}s.   Time2: {2}", ((DateTime.Now - start).TotalSeconds), time1, time2);
        }

        private static double time1 = 0;
        private static double time2 = 0;
        private static void ReadInChunks(string guid, int fromIndex, int toIndex)
        {
            DateTime t1 = DateTime.Now;

            RepositoryUtil.OpenRepository("petrel2", "test_repo");

            DateTime t2 = DateTime.Now;

            PointSet pointSet = RepoObject.ResolveUniqueId<PointSet>(guid);

            double[] xArray = null;
            double[] yArray = null;
            float[] zArray = null;

            var points = pointSet.Points.ToList();
            xArray = new double[toIndex - fromIndex + 1];
            yArray = new double[toIndex - fromIndex + 1];
            zArray = new float[toIndex - fromIndex + 1];
            for (int i = fromIndex; i < toIndex + 1; i++)
            {
                xArray[i - fromIndex] = points[i].X;
                yArray[i - fromIndex] = points[i].Y;
                zArray[i - fromIndex] = (float)points[i].Z;
            }

            DateTime t3 = DateTime.Now;

            RepositoryUtil.CloseRepository();

            DateTime t4 = DateTime.Now;
            time1 += (t4 - t1).TotalSeconds;
            time2 += (t3 - t2).TotalSeconds;
            Console.WriteLine("  Read Points: {0} -> {1}. Time1: {2}s, time2: {3}s", fromIndex+1, toIndex+1, (t4-t1).TotalSeconds, (t3-t2).TotalSeconds);
        }

        private static void RunReadWorkflow()
        {
            string[] guids = new string[] 
            {
                "6B737C4A009F4671AA9A1A1B6FAECA1E",
                "9B36CE11B9B743C3BC26AE3CF7B7EED2",
                "FA528AF1D628441F9103924C5E8F3E7C",
                "2D629550965F4E34B3387E281E878906",
                "314B2258107744819D6C2DE9504DEB45",
                "4521610D9B3E4C7986FD358D85FA2626"
            };

            foreach (string guid in guids)
            {
                //ReadPointSet(guid);
                ReadPointSet2(guid);
            }
        }

        private static void ReadPointSet2(string guid) // caches/saves list of points, and opens/closes repository once
        {
            time1 = 0;
            time2 = 0;
            DateTime start = DateTime.Now;

            RepositoryUtil.OpenRepository("petrel2", "test_repo");

            PointSet pointSet = RepoObject.ResolveUniqueId<PointSet>(guid);
            List<Point3> points = pointSet.Points.ToList();
            int totalPoints = points != null ? points.Count() : 0;
            Console.WriteLine("\nReading PointSet '" + pointSet.Name + "'. Points count: " + totalPoints + "\n");


            int fromIndex = 0;
            int toIndex = -1;
            int chunkSize = (1024 * 1024 * 10) / (8 + 8 + 4);
            do
            {
                fromIndex = toIndex + 1;
                toIndex = fromIndex + chunkSize - 1;

                if (toIndex > totalPoints - 1)
                    toIndex = totalPoints - 1;

                ReadInChunks2(points, fromIndex, toIndex);


            } while (toIndex < totalPoints - 1);

            RepositoryUtil.CloseRepository();
            Console.WriteLine("Total Read time: {0}s.   Time1: {1}s", ((DateTime.Now - start).TotalSeconds), time1);
        }

        private static void ReadInChunks2(List<Point3> points, int fromIndex, int toIndex)
        {
            DateTime t1 = DateTime.Now;

            double[] xArray = null;
            double[] yArray = null;
            float[] zArray = null;

            xArray = new double[toIndex - fromIndex + 1];
            yArray = new double[toIndex - fromIndex + 1];
            zArray = new float[toIndex - fromIndex + 1];
            for (int i = fromIndex; i < toIndex + 1; i++)
            {
                xArray[i - fromIndex] = points[i].X;
                yArray[i - fromIndex] = points[i].Y;
                zArray[i - fromIndex] = (float)points[i].Z;
            }

            DateTime t2 = DateTime.Now;
            time1 += (t2 - t1).TotalSeconds;
            Console.WriteLine("  Read Points: {0} -> {1}. Time1: {2}s", fromIndex + 1, toIndex + 1, (t2 - t1).TotalSeconds);
        }

        private static void WritePointSets(int num)
        {
            totalPoints = 100000;
            List<Point3> points = PrepareData(0, 1);
            Collection collection = RepoObject.Collections.Where(x => x.Name == "PointSet Collection").FirstOrDefault();
            if (collection == null)
            {
                collection = RepoObject.CreateCollection("PointSet Collection");
            }
            for (int i = 0; i < num; i++)
            {
                string id = i.ToString().PadLeft(4, '0');
                string name = "Pointset_" + id;
                Console.Out.Write("Creating PointSet {0}...", name);
                PointSet pointSet = collection.CreatePointSet(name);
                pointSet.Points = new Point3Set(points);
                ITypedFeature<string> geoName = pointSet.Extension.CreateAttribute<string>("CustomAttributes","geo_name");
                geoName.Value = "geo_" + id;
                Console.Out.Write("done! ... ");
                RepositoryUtil.SaveRepository(true);
            }
        }

        public static void Execute()
        {
            Console.WriteLine("\n===============================================");

            //int n = 1;
            //RunWorkflow(n);

            //RunReadWorkflow();

            RepositoryUtil.OpenRepository("petrel2", "test_repo2");

            WritePointSets(1000);

            RepositoryUtil.CloseRepository();

            Console.WriteLine("===============================================\n");
        }
    }
}
