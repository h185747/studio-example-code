﻿using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Well;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class PlannedTrajectoryWorkflow
    {
        private static Repository RepoObject => RepositoryUtil.RepoObject;
        private static string PlannedTrajectoryId { get; set; }

        private static void CreateSimplePlannedTrajectory(string name)
        {
            string wellbore_id = "67C6EF3DDA1A409EB9901E6C42614BB3"; // existing wellbore unique ID, in current repository
            Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(wellbore_id);

            Console.WriteLine("Creating Planned Trajectory '" + name + "'");

            PlannedTrajectory plannedTrajectory = borehole.Trajectory.TrajectoryCollection.CreateTrajectory<PlannedTrajectory>(name);
            PlannedTrajectoryId = plannedTrajectory.UniqueId;

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

            Console.WriteLine(name + "successfully created and saved!");
        }

        private static void AddWellPathSection1()
        {
            string wellbore_id = "67C6EF3DDA1A409EB9901E6C42614BB3"; // existing wellbore unique ID, in current repository
            Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(wellbore_id);

            TrajectoryCollection collection = borehole.Trajectory.TrajectoryCollection;
            PlannedTrajectory plannedTrajectory = (PlannedTrajectory)collection.Trajectories.Where(t => t.UniqueId.Equals(PlannedTrajectoryId)).First();

            plannedTrajectory.AutomaticRecalculationEnabled = true;

            plannedTrajectory.CreateSection<WellPathSectionCurveToTarget>().SetTvdDeltaXDeltaY(1000, 500, 500);

            Console.WriteLine("CaclulationSucceeded=" + plannedTrajectory.CalculationSucceeded.ToString());

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

            Console.WriteLine("WellPathSections created and saved!");
        }

        private static void AddWellPathSection2()
        {
            string wellbore_id = "67C6EF3DDA1A409EB9901E6C42614BB3"; // existing wellbore unique ID, in current repository
            Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(wellbore_id);

            TrajectoryCollection collection = borehole.Trajectory.TrajectoryCollection;
            PlannedTrajectory plannedTrajectory = (PlannedTrajectory)collection.Trajectories.Where(t => t.UniqueId.Equals(PlannedTrajectoryId)).First();

            plannedTrajectory.AutomaticRecalculationEnabled = true;

            plannedTrajectory.CreateSection<WellPathSectionCurveToTarget>().SetTvdDeltaXDeltaY(1500, -4737.519, 5734.891);

            Console.WriteLine("CaclulationSucceeded=" + plannedTrajectory.CalculationSucceeded.ToString());

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

            Console.WriteLine("WellPathSection2 created and saved!");
        }

        private static void AddWellPathSection3()
        {
            string wellbore_id = "67C6EF3DDA1A409EB9901E6C42614BB3"; // existing wellbore unique ID, in current repository
            Borehole borehole = RepoObject.ResolveUniqueId<Borehole>(wellbore_id);

            TrajectoryCollection collection = borehole.Trajectory.TrajectoryCollection;
            PlannedTrajectory plannedTrajectory = (PlannedTrajectory)collection.Trajectories.Where(t => t.UniqueId.Equals(PlannedTrajectoryId)).First();

            plannedTrajectory.AutomaticRecalculationEnabled = true;

            plannedTrajectory.CreateSection<WellPathSectionCurveToMDIncDls>().SetMdInclinationDlsAzimuth(15000, 1.5708, 0.0001745329, 4.161528162);

            Console.WriteLine("CaclulationSucceeded=" + plannedTrajectory.CalculationSucceeded.ToString());

            DatabaseSystem.RepositoryService.SaveRepository(RepoObject);

            Console.WriteLine("WellPathSection3 created and saved!");
        }

        public static void Execute()
        {
            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            CreateSimplePlannedTrajectory("PlannedTrajectory #1");
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            AddWellPathSection1();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            AddWellPathSection2();
            RepositoryUtil.CloseRepository();

            RepositoryUtil.OpenRepository("petrel2", "test_repo");
            AddWellPathSection3();
            RepositoryUtil.CloseRepository();
        }
    }
}