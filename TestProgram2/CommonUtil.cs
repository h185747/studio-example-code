﻿using Slb.Ocean.Core;
using Slb.Ocean.Studio.Data.Petrel.Internal;
using Slb.Ocean.Studio.Data.Petrel.IO.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProgram2
{
    class CommonUtil
    {
        public static Dictionary<string, string> GetExtendedProperties(DomainObjectBase obj, bool writeToConsole = false)
        {
            Dictionary<string, string> extendedProperties = new Dictionary<string, string>();
            ICustomAttributeService attrService = CoreSystem.GetService<ICustomAttributeService>();
            
            // old approach to fetch custom attributes using ICustomAttributeService
            IDictionary<CustomAttributeKey, string> customAttributes = attrService.GetAllAttributes(obj);
            if (writeToConsole)
                Console.WriteLine(" Printing extended properties for " + obj.GetType());
            foreach (KeyValuePair<CustomAttributeKey, string> kvp in customAttributes)
            {
                CustomAttributeKey key = kvp.Key;
                if (writeToConsole)
                    Console.WriteLine("    Key: " + key.AttributeNamespace + "-" + key.AttributeName + ", Value: " + kvp.Value);
                extendedProperties[key.AttributeName] = kvp.Value;
            }

            return extendedProperties;
        }

        public static void SetNewExtendedProperty(DomainObjectBase obj, string propertyName, string propertyValue)
        {
            ICustomAttributeService attrService = CoreSystem.GetService<ICustomAttributeService>();
            attrService.SetAttribute(obj, new CustomAttributeKey("CustomAttributes", propertyName), propertyValue);
        }
    }
}