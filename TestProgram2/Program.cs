﻿using Slb.Ocean.Studio.Data.Petrel;
using Slb.Ocean.Studio.Data.Petrel.Well;
using System;
using System.Linq;
using System.IO;
using System.Reflection;
using Slb.Ocean.Studio.Data.Petrel.Shapes;
using Slb.Ocean.Geometry;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TestProgram2
{
    class Program
    {
        private static Repository RepoObject { get; set; }
        static void Main(string[] args)
        {
            Console.WriteLine("Test Program started!");
           // Console.ReadKey();
            try
            {
                //OccupyLicense();
                Console.WriteLine("Before open repository :" + GC.GetTotalMemory(true));

                /* for (int counter = 0; counter < 20; counter++)
                 {*/
                //Console.WriteLine("Press ay key to start  ");
                //Console.ReadKey();
                //WellWorkflow.Execute();
                //LogCurveWorkflow.Execute();
                //wellObj.Dispose();
                // Console.WriteLine("\nWellbore bore Read " + counter);

                //}
                SeismicWorkflow.Execute();
                Console.WriteLine("\ncompleted");
                Console.ReadKey();
               

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Error while executing program...\n Exception: " + ex.Message);
            }
            finally
            {
                if (RepositoryUtil.RepoObject != null)
                    RepositoryUtil.CloseRepository();
            }
            Console.WriteLine("\n\nPress any key to exit...");
            Console.ReadKey();
        }

        private static void OccupyLicense()
        {
            Console.WriteLine();
            RepositoryUtil.OpenRepository("20", "equinor_issue", true);

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
           
        }
    }
}
